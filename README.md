# tankwar

#### 介绍
坦克大战游戏 

![设计模式笔记预览](https://images.gitee.com/uploads/images/2020/0504/155924_8806bce9_4932754.png "designpattern23.png")

主要技术思想与实践：
1. 设计模式实践 [(点这里查看笔记)](https://gitee.com/Sun_hongmin2313/tankwar/blob/master/src/main/java/com/tank/designstudy/DesignPatternNote1.0.md)
2. Netty网络框架 
3. 面向对象程序设计思想与实践 
4. java.awt.Frame 
5. 代码重构思想与实践 
6. IO、容器、多线程与线程池 
7. JUnit、JMH测试工具

#### 软件架构
JDK 1.8  
Spring 最新5.x系列  
Netty 5.0.x alpha系列  
IDEA  

#### 安装教程

1.  fork + clone即可  
2.  必要时可以 pull request 一下呦  
3.  别忘了 star  

#### 使用说明

1.  通过源码可以学习编码思想  
2.  可以通过 push history 循序渐进深入学习  
3.  可以通过pull request 来一起加入！！！  

#### 参与贡献

1.  Fork 本仓库  
2.  新建 Feat_xxx 分支  
3.  提交代码  
4.  新建 Pull Request  


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
=======
# tankwar

#### 介绍
坦克大战游戏主要技术栈：1.设计模式实践 2.Netty网络框架 3.面向对象程序设计思想与实践 4.java.awt.Frame 5.代码重构思想与实践 6.IO、容器、多线程与线程池 7.JUnit、JMH测试工具

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)