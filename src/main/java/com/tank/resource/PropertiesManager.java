package com.tank.resource;

import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
	// java1.0自带  方便就完事了
	static Properties props = new Properties();

	static {
		try {
			props.load(PropertiesManager.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取配置值
	 * @param key
	 * @return
	 */
	public static String get(String key){
		if(props == null){
			return null;
		}
		return props.get(key).toString();
	}

	/**
	 * 获取配置对象
	 * @param key
	 * @return
	 */
	public static Object getObject(String key){
		if(props == null){
			return null;
		}
		return props.get(key);
	}

}
