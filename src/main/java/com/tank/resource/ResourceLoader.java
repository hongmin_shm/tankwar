package com.tank.resource;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.tank.model.ImageUtil;

/**
 * @author 孙哈哈 资源加载器
 */
public class ResourceLoader {

	// public static BufferedImage tankL, tankU, tankR, tankD;
	public static BufferedImage vip_bad_tankL, vip_bad_tankU, vip_bad_tankR, vip_bad_tankD;
	public static BufferedImage vip_main_tankL, vip_main_tankU, vip_main_tankR, vip_main_tankD;
	public static BufferedImage bulletL, bulletU, bulletR, bulletD;
	public static BufferedImage vip_bulletL, vip_bulletU, vip_bulletR, vip_bulletD;
	public static List<BufferedImage> blasts = new ArrayList<>(16);

	static {
		try {
			// 加载坦克图片
			// A组坦克图片-四张图片分别表示坦克的上下左右四种不同方向
			/*
			 * tankL = ImageIO.read(ResourceLoader.class.getClassLoader().
			 * getResourceAsStream("images/tankL.gif")); tankU =
			 * ImageIO.read(ResourceLoader.class.getClassLoader().
			 * getResourceAsStream("images/tankU.gif")); tankR =
			 * ImageIO.read(ResourceLoader.class.getClassLoader().
			 * getResourceAsStream("images/tankR.gif")); tankD =
			 * ImageIO.read(ResourceLoader.class.getClassLoader().
			 * getResourceAsStream("images/tankD.gif"));
			 */
			// B组坦克-利用工具类将向上的一张图片旋转来得到不同角度的坦克图片
			vip_bad_tankU = ImageIO
					.read(ResourceLoader.class.getClassLoader().getResourceAsStream("static/images/BadTank1.png"));
			vip_bad_tankL = ImageUtil.rotateImage(vip_bad_tankU, -90);
			vip_bad_tankR = ImageUtil.rotateImage(vip_bad_tankU, 90);
			vip_bad_tankD = ImageUtil.rotateImage(vip_bad_tankU, 180);

			vip_main_tankU = ImageIO
					.read(ResourceLoader.class.getClassLoader().getResourceAsStream("static/images/GoodTank1.png"));
			vip_main_tankL = ImageUtil.rotateImage(vip_main_tankU, -90);
			vip_main_tankR = ImageUtil.rotateImage(vip_main_tankU, 90);
			vip_main_tankD = ImageUtil.rotateImage(vip_main_tankU, 180);

			// 加载子弹图片
			bulletU = ImageIO.read(ResourceLoader.class.getClassLoader().getResourceAsStream("static/images/bulletU.gif"));
			bulletL = ImageUtil.rotateImage(bulletU, -90);
			bulletR = ImageUtil.rotateImage(bulletU, 90);
			bulletD = ImageUtil.rotateImage(bulletU, 180);

			// 加载爆炸效果图片
			for (int i = 1; i <= 16; i++) {
				blasts.add(ImageIO
						.read(ResourceLoader.class.getClassLoader().getResourceAsStream("static/images/e" + i + ".gif")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
