package com.tank.cor;

import com.tank.model.GameObject;
import com.tank.model.Tank;

/**
 * @Author SunHongmin
 * @Description
 * @Date 2020/5/27 22:19
 */
public class TankTankCollider implements Collider {
    @Override
    public Boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof Tank && o2 instanceof Tank) {
            Tank tank1 = (Tank)o1;
            Tank tank2 = (Tank)o2;
            if (tank1.getRectangle().intersects(tank2.getRectangle())) {
                tank1.moveBack();
                tank2.moveBack();
            }
            // 坦克之间相撞  处理相持后  继续传给下一个责任链  因为有可能tank和子弹的碰撞处理还未完成
            return true;
        }
        // 未处理  交由责任链下一个节点
        return null;
    }
}
