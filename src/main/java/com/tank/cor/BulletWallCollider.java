package com.tank.cor;

import com.tank.model.Bullet;
import com.tank.model.GameObject;
import com.tank.model.Wall;

/**
 * @Author SunHongmin
 * @Description
 * @Date 2020/6/3 23:10
 */
public class BulletWallCollider implements Collider {
    @Override
    public Boolean collide(GameObject o1, GameObject o2) {
        if(o1 instanceof Bullet && o2 instanceof Wall){
            Bullet bullet = (Bullet)o1;
            Wall wall = (Wall)o2;
            if(bullet.getRectangle().intersects(wall.getRectangle())) {
                bullet.living = false;
            }
            return true;
        }else if(o1 instanceof Wall && o2 instanceof Bullet){
            Wall wall = (Wall)o1;
            Bullet bullet = (Bullet)o2;
            if(bullet.getRectangle().intersects(wall.getRectangle())) {
                bullet.living = false;
            }
            return true;
        }else{
            return null;
        }
    }
}
