package com.tank.cor;

import com.tank.model.GameObject;
import com.tank.model.Tank;
import com.tank.model.Wall;

/**
 * @Author SunHongmin
 * @Description
 * @Date 2020/6/3 23:10
 */
public class TankWallCollider implements Collider {

    @Override
    public Boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof Wall && o2 instanceof Tank) {
            Wall wall = (Wall)o1;
            Tank tank = (Tank)o2;
            if (tank.getRectangle().intersects(wall.getRectangle())) {
                tank.moveBack();
            }
            return true;
        } else if (o1 instanceof Tank && o2 instanceof Wall) {
            Tank tank = (Tank)o1;
            Wall wall = (Wall)o2;
            if (tank.getRectangle().intersects(wall.getRectangle())) {
                tank.moveBack();
            }
            return true;
        } else {
            return null;
        }

    }
}
