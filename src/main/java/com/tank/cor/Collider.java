package com.tank.cor;

import com.tank.model.GameObject;

/**
 * @Author SunHongmin
 * @Description 处理游戏中两个物体之间的碰撞
 * @Date 2020/5/25 23:02
 */
public interface Collider {
    /**
     * 处理o1 与  o2 之间的碰撞
     * @param o1
     * @param o2
     * @Return
     * 如果返回Boolean.TRUE，表示处理成功，停止处理链
     * 如果返回Boolean.FALSE，表示处理失败（请求被拒绝），停止处理链
     * 如果返回null，则交由下一个Handler处理。
     */
    Boolean collide(GameObject o1, GameObject o2);

}
