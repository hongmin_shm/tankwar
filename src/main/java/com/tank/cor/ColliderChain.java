package com.tank.cor;

import java.util.LinkedList;
import java.util.List;

import com.tank.model.GameObject;
import com.tank.resource.PropertiesManager;

/**
 * @Author SunHongmin
 * @Description 责任链模式 实现各种碰撞检测链来执行检测
 * 修订: 责任链也可以实现COllider接口 这样就可以将责任链之间进行嵌套（责任链同时也是一个节点）
 * @Date 2020/5/27 22:44
 */
public enum ColliderChain implements Collider {

    INSTANCE;

    private List<Collider> colliders = new LinkedList<Collider>();

    ColliderChain() {}

    public void addCollider(Collider collider) {
        colliders.add(collider);
    }

    public void initLoadingColliders() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        colliders.removeAll(colliders);
        String collidersStrs = PropertiesManager.get("colliders");
        for (String colliderStr : collidersStrs.split(",")) {
            Class<?> aClass = Class.forName(colliderStr);
            Collider o = (Collider)aClass.newInstance();
            colliders.add(o);
        }
    }

    @Override
    /**
     * 对于每一个ColliderHandler
     * 如果返回Boolean.TRUE，表示处理成功，停止处理链
     * 如果返回Boolean.FALSE，表示处理失败（请求被拒绝），停止处理链
     * 如果返回null，则交由下一个Handler处理。
     */
    public Boolean collide(GameObject o1, GameObject o2) {
        for (Collider collider : colliders) {
            Boolean collide = collider.collide(o1, o2);
            if (collide == null) {
                continue;
            } else {
                break;
            }

        }
        return true;
    }

}
