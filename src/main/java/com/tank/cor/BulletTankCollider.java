package com.tank.cor;

import com.tank.model.Bullet;
import com.tank.model.Explode;
import com.tank.model.GameModel;
import com.tank.model.GameObject;
import com.tank.model.Tank;

/**
 * @Author SunHongmin
 * @Description tank-bullet 碰撞
 * @Date 2020/5/25 23:05
 */
public class BulletTankCollider implements Collider {

    private GameModel gameModel = GameModel.INSTANCE;

    @Override
    public Boolean collide(GameObject o1, GameObject o2) {
        if (o1 instanceof Bullet && o2 instanceof Tank) {
            Bullet b = (Bullet)o1;
            Tank t = (Tank)o2;
            if (b.getGroup() != t.getGroup()) {
                if (b.getRectangle().intersects(t.getRectangle())) {
                    t.living = false;
                    b.living = false;
                    int realWidth = t.getX() + Tank.WIDTH / 2 - Explode.WIDTH / 2;
                    int realHeight = t.getY() + Tank.HEIGHT / 2 - Explode.HEIGHT / 2;
                    gameModel.addGameObject(new Explode(realWidth, realHeight, gameModel));
                }
            }
            // 相撞之后  坦克与子弹消亡  将退出后续的碰撞检测
            return false;
        } else if (o2 instanceof Bullet && o1 instanceof Tank) {
            collide(o2, o1);
            // 相撞之后  坦克与子弹消亡  将退出后续的碰撞检测
            return false;
        } else {
            // 未处理  交由责任链下一个节点
            return null;
        }
    }
}
