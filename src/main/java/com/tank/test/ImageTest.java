package com.tank.test;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;


import com.tank.model.Audio;
import com.tank.resource.PropertiesManager;

public class ImageTest {

	@Test
	public void test() {
		// fail("Not yet implemented");
		try {
			BufferedImage image = ImageIO
					.read(ImageTest.class.getClassLoader().getResourceAsStream("images/bulletD.gif"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testProperties(){
		new Audio("audio/tank_fire.wav").play();
		System.out.println(PropertiesManager.get("initTankCount"));
	}

}
