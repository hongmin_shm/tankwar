package com.tank.frame;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import com.tank.model.GameObject;

/**
 * @Author SunHongmin
 * @Description 统计游戏画面信息
 * @Date 2020/6/3 23:11
 */
public class GameMonitor extends GameObject {

    private Map<GameObject, Integer> container = new HashMap<>(512);
    private Map<Class, InnerModel> container1 = new HashMap<>(512);

    private GameMonitor() {}

    public static GameMonitor getInstance() {
        return Instance.gameMonitor;
    }

    public void addCountModel(Class key, int width, int height, String description) {
        container1.put(key, new InnerModel(width, height, description));
    }

    public void addCountModel(GameObject... keys) {
        for (GameObject key : keys) {
            container.put(key, 0);
        }
    }

    public void countModels(GameObject gameObject) {
        if (!container1.containsKey(gameObject.getClass())) {
            return;
        }
        container1.get(gameObject.getClass()).addCount();
    }

    private static class Instance {
        private static final GameMonitor gameMonitor = new GameMonitor();
    }

    @Override
    public void paint(Graphics g) {
        Color source = g.getColor();
        g.setColor(Color.WHITE);
        container1.forEach((k, v) -> {
            g.drawString(v.description + "：" + v.count, v.width, v.height);
            v.refrashCount();
        });
        g.setColor(source);
    }

    @Override
    public boolean getLiving() {
        return true;
    }

    class InnerModel {
        public int width, height;
        public String description;
        public int count;

        public InnerModel(int width, int height, String description) {
            this.width = width;
            this.height = height;
            this.description = description;
        }

        public void addCount() {
            count++;
        }

        public void refrashCount() {
            count = 0;
        }
    }

}
