package com.tank.designstudy.builder;

/**
 * @author 孙哈哈
 *
 */
/**
 * @author 孙哈哈
 *
 */
public class BuilderWXStudy02 {

}

/**
 * 产品类
 */
class ProductII {
	private String name;
	private Integer val;

	public ProductII(BuilderII builder) {
		this.name = builder.name;
		this.val = builder.val;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", val=" + val + "]";
	}

}

/**
 * 抽象建造者
 */
abstract class BuilderII {
	protected Integer val;
	protected String name;

	// 设置产品不同部分，以获得不同的产品
	public abstract void setVal(Integer val);

	// 设置名字 公用方法
	public void setName(String name) {
		this.name = name;
	}

	// 建造产品
	public abstract ProductII buildProduct();
}

/**
 *	具体建造者
 */
class ConcreateBuilderII extends BuilderII {

	@Override
	public void setVal(Integer val) {
		/**
		 * 产品类内部逻辑 实际存储的值是 val + 100
		 */
		this.val = val + 100;
	}

	// 组建一个产品
	@Override
	public ProductII buildProduct() {
		// 这块还可以写特殊的校验逻辑
		return new ProductII(this);
	}

}
// 导演类
class DirectorII{
	private Builder builder = new ConcreateBuilder();
	public Product getAProduct() {
		builder.setName("ProductA");
		builder.setVal(2);
		return builder.buildProduct();
	}
}


