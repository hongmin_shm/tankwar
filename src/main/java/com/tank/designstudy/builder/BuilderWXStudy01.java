package com.tank.designstudy.builder;

/**
 * @author 孙哈哈
 *
 */
/**
 * @author 孙哈哈
 *
 */
public class BuilderWXStudy01 {

}

/**
 * 产品类
 */
class Product {
	private String name;
	private Integer val;

	public Product(String name, Integer val) {
		this.name = name;
		this.val = val;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", val=" + val + "]";
	}

}

/**
 * 抽象建造者
 */
abstract class Builder {
	protected Integer val;
	protected String name;

	// 设置产品不同部分，以获得不同的产品
	public abstract void setVal(Integer val);

	// 设置名字 公用方法
	public void setName(String name) {
		this.name = name;
	}

	// 建造产品
	public abstract Product buildProduct();
}

/**
 *	具体建造者
 */
class ConcreateBuilder extends Builder {

	@Override
	public void setVal(Integer val) {
		/**
		 * 产品类内部逻辑 实际存储的值是 val + 100
		 */
		this.val = val + 100;
	}

	// 组建一个产品
	@Override
	public Product buildProduct() {
		// 这块还可以写特殊的校验逻辑
		return new Product(name, val);
	}

}
// 导演类
class Director{
	private Builder builder = new ConcreateBuilder();
	public Product getAProduct() {
		builder.setName("ProductA");
		builder.setVal(2);
		return builder.buildProduct();
	}
}


