# GOF 23种设计模式
[TOC]
## 1.创建型模式
### 1.1 工厂方法（与静态工厂）
> 定义一个用于创建对象的接口，让子类决定实例化哪一个类。Factory Method使一个类的实例化延迟到其子类。
+ 工厂方法
```Java
import java.math.BigDecimal;

/**
 * @author sunhongmin 工厂方法的目的是使得创建对象和使用对象是分离的，并且客户端总是引用抽象工厂和抽象产品
 */
public class FactoryMethodStudy {
	public static void main(String[] args) {
		//调用方可以完全忽略真正的工厂NumberFactoryImpl和实际的产品BigDecimal，
		//这样做的好处是允许创建产品的代码独立地变换，而不会影响到调用方。
		NumberFactory n = NumberFactory.getInstance();
		Number number = n.parse("3.1415926");
		System.out.println(number);
	}
}

//用接口更合适，这里为了写静态方法实现，1.8未支持
abstract class NumberFactory {
	
	// 生产抽象产品
	public abstract Number parse(String str);

	// 生产工厂实例对象
	public static NumberFactory getInstance() {
		return instance;
	}
	
	//持有实例对象
	static NumberFactory instance = new NumberFactoryDefaultImpl();
	
}
// 具体工厂实现
class NumberFactoryDefaultImpl extends NumberFactory {
	//生产实际产品  BigDecimal 继承自 Number
    @Override
	public Number parse(String str) {
		return new BigDecimal(str);
	}
}
```
+ 静态工厂方法
```Java
import java.math.BigDecimal;

/**
 * @author sunhongmin 使用静态方法创建产品的方式成为静态工厂方法
 */
public class StaticFactoryMethodStudy {
	public static void main(String[] args) {
		Number number = NumberFactoryII.parse("3.1415926");
		System.out.println(number);
		/**
		 * 静态工厂方法广泛应用在java标准库中，例如Integer n = Integer.valueOf(100);
		 * Integer既是产品又是静态工厂，在valueOf方法中，可以有效利用缓冲来直接返回Integer实例(-128~127)，而对于调用者
		 * 显然没必要知道这些细节 
		 * public static Integer valueOf(int i) { 
		 * if (i >= IntegerCache.low && i <= IntegerCache.high) {
		 * 	return IntegerCache.cache[i + (-IntegerCache.low)]; 
		 * }
		 * return new Integer(i); 
		 * }
		 * *工厂方法可以隐藏创建产品的细节，且不一定每次都会真正创建产品，完全可以返回缓存的产品，从而提升速度并减少内存消耗。
		 */
	}
}

// 大多数情况并不必要使用抽象工厂方法，可以使用静态方法直接返回产品

class NumberFactoryII {
	public static Number parse(String str) {
		Integer.valueOf(123);
		return new BigDecimal(str);
	}
}
```
+ 小结  
工厂方法指定义工厂接口和产品接口，但如何创建实际工厂和实际产品被推迟到子类实现，从而使调用方只和抽象工厂和抽象产品打交道。  
实际更常用的是更简单的静态工厂方法，它允许工厂内部对创建产品进行优化。  
调用方尽量持有接口或抽象类，避免持有具体类型的子类，以便工厂方法能随时切换不同的子类返回，却不影响调用方代码。  
### 1.2 抽象工厂
> 提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。  
```Java
import java.io.IOException;

//不光工厂是抽象的，产品也是抽象的
/**
 * @author sunhongmin
 *         不但工厂是抽象的，产品是抽象的，而且有多个产品需要创建，因此，这个抽象工厂会对应到多个实际工厂，每个实际工厂负责创建多个实际产品
 *
 */
public class AbstractFactoryStudy {
	public static void main(String[] args) throws IOException {
		// 实例化第一家公司
		AbstractFactory factory = new FastFactory();
		HtmlDocument html = factory.createHtml("note.md");
		html.toHtml();
		html.save("htmlpath");
		WordDocument word = factory.createWord("note.md");
		word.save("wordpath");

		////// 当写完了第二家公司的实现后，就可以灵活的切换不同的实现了 注意 只需要给一个new就可以了
		// 当进一步把new 的逻辑用工厂方法实现，那么就真正实现了灵活切换
		AbstractFactory factory2 = new GoogleFactory();
		HtmlDocument html2 = factory.createHtml("note.md");
		html.toHtml();
		html.save("htmlpath");
		WordDocument word2 = factory.createWord("note.md");
		word.save("wordpath");
	}

}

/**
 * @author sunhongmin 抽象工厂类，提供另两个产品工厂的生产
 */
interface AbstractFactory {
	HtmlDocument createHtml(String md);

	WordDocument createWord(String md);
}

// html 抽象产品工厂，提供html转换和保存
interface HtmlDocument {
	String toHtml();

	void save(String path) throws IOException;
}

// word 抽象产品工厂，提供word转换和保存
interface WordDocument {
	void save(String path) throws IOException;
}

// 第一家提供商，需要实现html抽象工厂和word抽象工厂01
class FastHtmlDocument implements HtmlDocument {
	private String md;

	public FastHtmlDocument(String md) {
		this.md = md;
	}

	@Override
	public String toHtml() {
		System.out.println("TO HTML ...");
		return "FORMAT SUCCESS";
	}

	@Override
	public void save(String path) throws IOException {
		System.out.println("SAVE HTML TO PATH ... " + path);
	}
}

// 第一家提供商，需要实现html抽象工厂和word抽象工厂02
class FastWordDocument implements WordDocument {
	private String md;

	public FastWordDocument(String md) {
		this.md = md;
	}

	@Override
	public void save(String path) throws IOException {
		System.out.println("SAVE WORD TO PATH ... " + path);
	}
}

// 第一家提供商的产品工厂，用来生产产品提供转换服务
class FastFactory implements AbstractFactory {
	@Override
	public HtmlDocument createHtml(String md) {
		return new FastHtmlDocument(md);
	}

	@Override
	public WordDocument createWord(String md) {
		return new FastWordDocument(md);
	}
}

// =====分割线，代码写到这，至少可以实现整个产品逻辑了，下面的代码更近一步，展示设计模式的一大原则，对修改关闭，对拓展开放
// 第二家提供商，同样需要实现html抽象工厂和word抽象工厂01
class GooleHtmlDocument implements HtmlDocument {
	private String md;

	public GooleHtmlDocument(String md) {
		this.md = md;
	}

	@Override
	public String toHtml() {
		System.out.println("GooleHtmlDocument TO HTML ...");
		return "GooleHtmlDocument FORMAT SUCCESS";
	}

	@Override
	public void save(String path) throws IOException {
		System.out.println("GooleHtmlDocument SAVE HTML TO PATH ... " + path);
	}
}

// 第二家提供商，同样实现html抽象工厂和word抽象工厂02
class GooleWordDocument implements WordDocument {
	private String md;

	public GooleWordDocument(String md) {
		this.md = md;
	}

	@Override
	public void save(String path) throws IOException {
		System.out.println("GooleWordDocument SAVE WORD TO PATH ... " + path);
	}
}

// 第二家提供商的产品工厂，用来生产产品提供转换服务
class GoogleFactory implements AbstractFactory {
	@Override
	public HtmlDocument createHtml(String md) {
		return new GooleHtmlDocument(md);
	}

	@Override
	public WordDocument createWord(String md) {
		return new GooleWordDocument(md);
	}
}
```
+ 小结  
抽象工厂模式是为了让创建工厂和一组产品与使用相分离，并可以随时切换到另一个工厂以及另一组产品  
抽象工厂模式实现的关键点是定义工厂接口和产品接口，但如何实现工厂与产品本身需要留给具体子类实现，客户端只和抽象工厂和抽象产品打交道  
+ 工厂方法模式与抽象工厂模式的区别
工厂方法模式只能创建一个类的产品，抽象工厂模式可以创建一系列产品

### 1.3 建造者（生成器）

> 将一个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示

+ 什么时候适合使用建造者模式？

  1. 创建对象参数过多的时候  

     创建一个有很多属性的对象，如果参数在构造方法中写，看起来很乱，容易写错  

  2. 对象的部分属性是可选择的时候
  
     创建的对象有很多属性是可选择的那种，常见的比如配置类等，不同使用者有不同的配置
  
  3. 对象创建完成后，就不能修改内部属性的时候
      
      不提供set()方法，使用建造者模式一次性把对象创建完成  
      
+ 建造者模式和工厂模式的区别是什么？

### 1.4 原型
### 1.5 单例
## 2.结构型模式
### 2.1 适配器
### 2.2 桥接
### 2.3 组合
### 2.4 装饰器
### 2.5 外观
### 2.6 享元
### 2.7 代理
## 3.行为型模式
### 3.1 责任链
### 3.2 命令
### 3.3 解释器
### 3.4 迭代器
### 3.5 中介
### 3.6 备忘录
### 3.7 观察者
### 3.8 状态
### 3.9 策略
### 3.10 模板方法
### 3.11 访问者

---
### 调停者（消息中间件）
多个系统之间相互联系，可以抽离出独立系统来维护关系，这样新加入者就可以简单的接入

model view 分离 坦克类不再于tankframe耦合 

### 门面模式
> 应用：新建游戏模型类，负责游戏内所有组件的门面，对外部来说提供了统一入口

