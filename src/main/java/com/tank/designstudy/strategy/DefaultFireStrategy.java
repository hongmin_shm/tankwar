package com.tank.designstudy.strategy;

import com.tank.model.Audio;
import com.tank.model.Bullet;
import com.tank.model.GroupEnum;
import com.tank.model.Tank;

/**
 * @author 孙哈哈
 *	默认实现 开火策略
 */
public class DefaultFireStrategy implements FireStrategy{

	public static class DefaultFireStrategyHolder{
		public final static DefaultFireStrategy INSTANCE = new DefaultFireStrategy();
	}

	private DefaultFireStrategy(){}

	public static DefaultFireStrategy getInstance(){
		return DefaultFireStrategyHolder.INSTANCE;
	}

	@Override
	public void fire(Tank t) {
		//这里可以灵活调整游戏内所有坦克开火的方式  炮弹方向  音效等
		int realW = t.getX() + Tank.WIDTH / 2 - Bullet.WIDTH / 2;
		int realH = t.getY() + Tank.HEIGHT / 2 - Bullet.HEIGHT / 2;
		//this.frame.bullets.add(new Bullet(realW, realH + 5, this.dir, this.group, this.frame));
		new Bullet(realW,realH,t.getDir(),t.getGroup());
		// 开火
		if(t.getGroup() == GroupEnum.GOOD){
			/*new Thread(()->{
				new Audio("static/audio/tank_fire.wav").play();
			}).start();*/
		}
	}

}
