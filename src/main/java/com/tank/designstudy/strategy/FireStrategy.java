package com.tank.designstudy.strategy;

import com.tank.model.Tank;

/**
 * @author 孙哈哈
 *	策略模式
 */
public interface FireStrategy {
	// 定义一个开火策略
	void fire(Tank t);
}
