package com.tank.designstudy.singleton;

/**
 * @author 孙哈哈 
 * 使用静态内部类实现单例设计模式
 */
public class Singleton01 {

	/**
	 * @author 孙哈哈
	 *	Singleton01 的静态内部类，相较于饿汉式，外部类被加载时，并不会马上加载内部类，在使用到的时候才会加载，实现了按需加载
	 *	jvm 对Singleton01的加载保证只有一次，所以静态内部类也只会加载一次，保证了instance的唯一
	 */
	public static class SingletonHolder {
		private final static Singleton01 INSTANCE = new Singleton01();
	}

	private Singleton01() {}
	
	public static Singleton01 getInstance(){
		return SingletonHolder.INSTANCE;
	}
	
	
}
