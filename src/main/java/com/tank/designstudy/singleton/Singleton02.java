package com.tank.designstudy.singleton;

/**
 * @author 孙哈哈
 *	effective java 中推荐写法
 *	解决了线程同步  和  防止反序列化破坏单例
 */
public enum Singleton02 {
	INSTANCE;
}


/***
 * 这里补充下防止反射破坏单例的方式  添加readResolve方法
 * private Object readResolve(){
        return hungrySingleton;
    }
 * 
 * 反序列化过程中会调用readResolve方法
 * 注意Object类中并没有readResolve方法
 */
