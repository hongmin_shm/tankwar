package com.tank.designstudy.factorymethod;

import java.math.BigDecimal;

/**
 * @author sunhongmin 工厂方法的目的是使得创建对象和使用对象是分离的，并且客户端总是引用抽象工厂和抽象产品
 */
public class FactoryMethodStudy {
	public static void main(String[] args) {
		//调用方可以完全忽略真正的工厂NumberFactoryImpl和实际的产品BigDecimal，
		//这样做的好处是允许创建产品的代码独立地变换，而不会影响到调用方。
		NumberFactory n = NumberFactory.getInstance();
		Number number = n.parse("3.1415926");
		System.out.println(number);
	}
}

//用接口更合适，这里为了写静态方法实现，1.8未支持
abstract class NumberFactory {
	
	// 生产抽象产品
	public abstract Number parse(String str);

	// 生产工厂实例对象
	public static NumberFactory getInstance() {
		return instance;
	}
	
	//持有实例对象
	static NumberFactory instance = new NumberFactoryDefaultImpl();
	
}
// 具体工厂实现
class NumberFactoryDefaultImpl extends NumberFactory {
	//生产实际产品  BigDecimal 继承自 Number
    @Override
	public Number parse(String str) {
		return new BigDecimal(str);
	}
}
