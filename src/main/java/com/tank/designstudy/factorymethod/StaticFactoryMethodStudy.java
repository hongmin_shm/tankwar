package com.tank.designstudy.factorymethod;

import java.math.BigDecimal;

/**
 * @author sunhongmin 使用静态方法创建产品的方式成为静态工厂方法
 */
public class StaticFactoryMethodStudy {
	public static void main(String[] args) {
		Number number = NumberFactoryII.parse("3.1415926");
		System.out.println(number);
		/**
		 * 静态工厂方法广泛应用在java标准库中，例如Integer n = Integer.valueOf(100);
		 * Integer既是产品又是静态工厂，在valueOf方法中，可以有效利用缓冲来直接返回Integer实例(-128~127)，而对于调用者
		 * 显然没必要知道这些细节 
		 * public static Integer valueOf(int i) { 
		 * if (i >= IntegerCache.low && i <= IntegerCache.high) {
		 * 	return IntegerCache.cache[i + (-IntegerCache.low)]; 
		 * }
		 * return new Integer(i); 
		 * }
		 * *工厂方法可以隐藏创建产品的细节，且不一定每次都会真正创建产品，完全可以返回缓存的产品，从而提升速度并减少内存消耗。
		 */
	}
}

// 大多数情况并不必要使用抽象工厂方法，可以使用静态方法直接返回产品

class NumberFactoryII {
	public static Number parse(String str) {
		Integer.valueOf(123);
		return new BigDecimal(str);
	}
}
