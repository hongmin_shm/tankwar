package com.tank.model;

import java.awt.*;

/**
 * @Author SunHongmin
 * @Description 游戏组件公共父类
 * @Date 2020/5/8 22:23
 */
public abstract class GameObject {
    public int x,y;

    public boolean living;

    public abstract void paint(Graphics g);

    public abstract boolean getLiving();


}
