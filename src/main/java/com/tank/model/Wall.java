package com.tank.model;

import java.awt.*;

/**
 * @Author SunHongmin
 * @Description
 * @Date 2020/6/3 22:45
 */
public class Wall extends GameObject{

    private int x,y;
    private int width,height;

    public boolean living = true;

    private Rectangle rectangle = new Rectangle();


    public Wall(int x,int y,int width,int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        rectangle.x = x;
        rectangle.y = y;
        rectangle.width = width;
        rectangle.height = height;
        // 实例化时添加到gamemodel中
        GameModel.INSTANCE.addGameObject(this);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public void paint(Graphics g) {
        Color sourceColor = g.getColor();
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x,y,width,height);
        g.setColor(sourceColor);
    }

    @Override
    public boolean getLiving() {
        return living;
    }
}
