package com.tank.model;

/**
 * @author 孙哈哈
 *1.0 方向枚举类  上下左右和静止
 *1.1 去除静止枚举，方向中不应包含运动状态，改由tank类只有运动属性
 */
public enum DirEnum {
	UP, DOWN, LEFT, RIGHT
}
