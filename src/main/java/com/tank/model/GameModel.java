package com.tank.model;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import com.tank.cor.ColliderChain;
import com.tank.frame.GameMonitor;

/**
 * @Author SunHongmin
 * @Description 游戏组件的组织者  相当于mvc当中的 model -tankframe只和GameModel打交道  -门面模式
 * @Date 2020/5/8 21:19
 */
public enum GameModel {
    INSTANCE;
    // 这里设置访问级别为public 因为游戏画面属性会被经常访问
    public static int GAME_WIDTH;
    public static int GAME_HEIGHT;
    // 主战坦克
    private Tank tank1;
    // 更改为持有公共父类,这里用LinkedList是因为这个对象的成员需要频繁的插入和删除
    private List<GameObject> objects = new LinkedList<>();

    private GameMonitor gameMonitor;

    // 碰撞责任链
    private ColliderChain colliderChain;

    GameModel() {}

    public void paint(Graphics g) {
        // 主战坦克被消灭 游戏结束
        // GAME WIN

        // 由坦克类负责绘制坦克
        tank1.paint(g);

        for (int i = 0; i < objects.size(); i++) {
            GameObject gameObject = objects.get(i);
            if (!gameObject.getLiving()) {
                objects.remove(i);
                i--;
                continue;
            }
            gameMonitor.countModels(gameObject);
            gameObject.paint(g);
        }

        //判断游戏物体之间的碰撞
        for (int i = 0; i < objects.size(); i++) {
            for (int j = i + 1; j < objects.size(); j++) {
                colliderChain.collide(objects.get(i), objects.get(j));
            }
        }
    }

    public Tank getMainTank() {
        return tank1;
    }

    public void addGameObject(GameObject gameObject) {
        objects.add(gameObject);
    }

    public void addGameObjectAll(List<GameObject> gameObjects) {
        objects.addAll(gameObjects);
    }

    public void setTank1(Tank tank1) {
        this.tank1 = tank1;
    }

    public void setObjects(List<GameObject> objects) {
        this.objects = objects;
    }

    public void setGameMonitor(GameMonitor gameMonitor) {
        this.gameMonitor = gameMonitor;
    }

    public void setColliderChain(ColliderChain colliderChain) {
        this.colliderChain = colliderChain;
    }
}
