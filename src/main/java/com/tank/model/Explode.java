package com.tank.model;

import java.awt.Graphics;

import com.tank.resource.ResourceLoader;

/**
 * @author 孙哈哈 爆炸效果类
 */
public class Explode extends GameObject {
    // 宽高
    public static int WIDTH = ResourceLoader.blasts.get(0).getWidth();
    public static int HEIGHT = ResourceLoader.blasts.get(0).getHeight();
    // 坐标
    private int x, y;
    // 是否播放完成
    public boolean living = true;
    private GameModel gameModel = null;

    private int step = 15;

    //private Audio audio = new Audio("static/audio/explode.wav");

    public Explode(int x, int y, GameModel gameModel) {
        this.x = x;
        this.y = y;
        this.gameModel = gameModel;
        new Thread(() -> {
            audioPlay();
        }).start();
    }

    private void audioPlay() {
        /*audio.play();*/
    }

    @Override
    public boolean getLiving() {
        return this.living;
    }

    @Override
    public void paint(Graphics g) {
        if(!this.living){
            return;
        }
        g.drawImage(ResourceLoader.blasts.get(step--), x, y, null);
        if (step <= 0) {
            this.living = false;
        }
    }
}
