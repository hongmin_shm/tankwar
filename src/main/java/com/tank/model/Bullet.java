package com.tank.model;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.tank.resource.PropertiesManager;
import com.tank.resource.ResourceLoader;

/**
 * @author 孙哈哈 子弹类 1.2 add field live\frame
 */
public class Bullet extends GameObject {
    // 速度
    private static int speed = 30;
    // 子弹宽高
    public final static int WIDTH = ResourceLoader.bulletD.getWidth();
    public final static int HEIGHT = ResourceLoader.bulletD.getHeight();
    // 坐标
    private int x, y;
    // 方向
    private DirEnum dir;

    // 添加子弹在游戏中的存活状态
    public boolean living = true;
    // 持有tankFrame引用
    //private GameModel gameModel;
    // 分组
    private GroupEnum group = GroupEnum.BAD;

    // 子弹边界
    private Rectangle rectangle = new Rectangle();

    public Bullet(int x, int y, DirEnum dir, GroupEnum group) {
        super();
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.group = group;
        int bulletSpeed = Integer.parseInt(PropertiesManager.get("bulletSpeed"));
        this.speed = bulletSpeed;
        //gameModel.getBullets().add(this);
        updateRectanglePosition(rectangle);

    }

    /**
     * 更新子弹矩形模型
     *
     * @param rectangle
     */
    private void updateRectanglePosition(Rectangle rectangle) {
        rectangle.width = Bullet.WIDTH;
        rectangle.height = Bullet.HEIGHT;
        rectangle.x = this.x;
        rectangle.y = this.y;
    }

    /**
     * 动作方法-移动子弹
     */
    private void move() {
        /*
         * if (!live) { frame.bullets.remove(this); return; }
         */
        switch (dir) {
            case LEFT:
                x -= speed;
                break;
            case RIGHT:
                x += speed;
                break;
            case UP:
                y -= speed;
                break;
            case DOWN:
                y += speed;
                break;
            default:
                break;
        }
        if (x < 0 || y < 0 || x > GameModel.GAME_WIDTH || y > GameModel.GAME_HEIGHT) {
            this.living = false;
        }
        updateRectanglePosition(rectangle);
    }

    @Override
    public void paint(Graphics g) {
        if (!this.living) {
            return;
        }
        switch (dir) {
            case LEFT:
                g.drawImage(ResourceLoader.bulletL, x, y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceLoader.bulletR, x, y, null);
                break;
            case UP:
                g.drawImage(ResourceLoader.bulletU, x, y, null);
                break;
            case DOWN:
                g.drawImage(ResourceLoader.bulletD, x, y, null);
                break;
            default:
                break;
        }
        // 移动
        move();
    }

    public GroupEnum getGroup() {
        return group;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public boolean getLiving() {
        return this.living;
    }

    @Override
    public String toString() {
        return "Bullet [x=" + x + ", y=" + y + ", dir=" + dir + ", width=" + WIDTH + ", height=" + HEIGHT + "]";
    }
}
