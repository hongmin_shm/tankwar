package com.tank.start;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.tank.cor.ColliderChain;
import com.tank.frame.GameMonitor;
import com.tank.frame.TankFrame;
import com.tank.model.Audio;
import com.tank.model.Bullet;
import com.tank.model.GameModel;
import com.tank.model.GroupEnum;
import com.tank.model.Tank;
import com.tank.model.Wall;
import com.tank.resource.PropertiesManager;

/**
 * 坦克游戏启动类 入口
 */
public class TankWar {

    // 游戏数据监控工具
    private static GameMonitor gameMonitor = GameMonitor.getInstance();
    // 碰撞责任链
    private static ColliderChain colliderChain = ColliderChain.INSTANCE;
    // 线程池
    private static ExecutorService pool = Executors.newFixedThreadPool(8);

    /**
     * 游戏启动入口
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws Exception {
        // 游戏模型类
        GameModel gameModel = GameModel.INSTANCE;
        // 游戏宽高
        int GAME_WIDTH = Integer.parseInt(PropertiesManager.get("gameWidth"));
        int GAME_HEIGHT = Integer.parseInt(PropertiesManager.get("gameHeight"));
        // 主战坦克速度
        int mainTankSpeed = Integer.parseInt(PropertiesManager.get("mainTankSpeed"));
        // 主战坦克
        new Tank(100, GAME_HEIGHT - Tank.HEIGHT, mainTankSpeed, GroupEnum.GOOD);
        // 更改为持有公共父类,这里用LinkedList是因为这个对象的成员需要频繁的插入和删除

        int initBadTanks = Integer.parseInt(PropertiesManager.get("initTankCount"));
        int badTankSpeed = Integer.parseInt(PropertiesManager.get("badTankSpeed"));
        // 添加敌方坦克，通过配置获取
        for (int i = 0; i < initBadTanks; i++) {
            new Tank(100 + i * 50, GAME_HEIGHT - 300, badTankSpeed, GroupEnum.BAD);
        }

        new Wall(100, 200, 150, 50);
        new Wall(500, 200, 150, 50);
        new Wall(250, 200, 50, 300);
        new Wall(450, 200, 50, 300);

        gameModel.GAME_WIDTH = GAME_WIDTH;
        gameModel.GAME_HEIGHT = GAME_HEIGHT;
        // 初始化游戏监视器
        gameMonitor.addCountModel(Tank.class, 10, 80, "敌方坦克数量");
        gameMonitor.addCountModel(Bullet.class, 10, 100, "子弹数量");
        gameModel.addGameObject(gameMonitor);

        // 初始化游戏模型碰撞处理链
        colliderChain.initLoadingColliders();
        gameModel.setColliderChain(colliderChain);

        gameModel.setGameMonitor(gameMonitor);

        // 启动窗口
        TankFrame tf = new TankFrame(gameModel);
        //BGM
        pool.execute(() -> new Audio("static/audio/war1.wav").loop());
        // 模拟屏幕刷新
        while (true) {
            Thread.sleep(100);
            tf.repaint();
        }
    }

    /**
     * TODO list
     *  + 将游戏内线程任务交由线程池处理
     */

}
